const path = require('path');
const Jimp = require('jimp');
const fs = require('fs');

/*exports.run = (client, message, args) => {
	let user = message.mentions.users.first() || message.author;
	if(!user.avatar) return;

	Jimp.read(path.join(__dirname, '../../../img', 'internet01.png'), (err, img1)=>{
		if(err) throw err;
		Jimp.read(message.embeds[0] ? message.embeds[0].url : 'https://cdn.discordapp.com/avatars/'+user.id+'/'+user.avatar+'.png', (err, img2)=>{
			if(err) throw err;
			Jimp.read(path.join(__dirname, '../../../img', 'internet02.png'), (err, img3)=>{
				if(err) throw err;
				// img1.composite(img2.resize(880, 720), 380, 1303).composite(img3,0,0).getBuffer(Jimp.MIME_PNG, (err, buffer)=>{
				img1.composite(img2.resize(220, 180), 95, 326).composite(img3,0,0).getBuffer(Jimp.MIME_PNG, (err, buffer)=>{
					if(err) throw err;
					message.channel.sendFile(buffer, 'internet.png').then()
						.catch((err) => { if(err) throw err; });
					});
			});
		});
	});
}*/

exports.run = async (client, message, args) => {
	let user = message.mentions.users.first() || message.author;
	if(!user.avatar) return;
	
	console.log(user);
	console.log(message);
	console.log('message.embeds: ', message.embeds[0] ? 'THERE IS AN EMBED' : 'THERE IS NO EMBED');
	
	let jimp2 = await Jimp.read(message.embeds[0] ? message.embeds[0].url : 'https://cdn.discordapp.com/avatars/'+user.id+'/'+user.avatar+'.png');
	let jimp1 = await Jimp.read(path.join(__dirname, '../../../img', 'internet01.png'));
	let jimp3 = await Jimp.read(path.join(__dirname, '../../../img', 'internet02.png'));
	let composite = await jimp1.composite(jimp2.resize(220,180), 95, 326).composite(jimp3,0,0);
	
	composite.getBuffer(Jimp.MIME_PNG, (err, buffer)=>{
		if(err){throw err;}
		message.channel.sendFile(buffer, 'internet.png')
			.then()
			.catch((err) => {if(err) throw err;});
	});
}
const path = require('path');
const Jimp = require('jimp');

exports.run = async (client, message, args) => {
	let user = message.mentions.users.first() || message.author;
	if(!user.avatar) return;
	
	console.log(user);
	console.log(message);
	console.log('message.embeds: ', message.embeds[0] ? 'THERE IS AN EMBED' : 'THERE IS NO EMBED');
	
	let jimp2 = await Jimp.read(message.embeds[0] ? message.embeds[0].url : 'https://cdn.discordapp.com/avatars/'+user.id+'/'+user.avatar+'.png');
	let jimp1 = await Jimp.read(path.join(__dirname, '../../../img', 'challenger.png'));
	let composite = await jimp1.composite(jimp2.resize(150,150), 420, 250);
	
	composite.getBuffer(Jimp.MIME_PNG, (err, buffer)=>{
		if(err){throw err;}
		message.channel.sendFile(buffer, 'challenger.png')
			.then()
			.catch((err) => {if(err) throw err;});
	});
}
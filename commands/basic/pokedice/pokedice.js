const Chance = require('chance');
const chance = new Chance();

const pokedex = new function(){
	this.min = 1;
	this.max = 802;
	this.gen1 = {
		min: 1, max: 151,
		gen: 1,
		name: "Kanto",
		shortcut: "gen1"
	};
	this.gen2 = {
		min: 152, max: 251,
		gen: 2,
		name: "Johto",
		shortcut: "gen2"
	};
	this.gen3 = {
		min: 252, max: 386,
		gen: 3,
		name: "Hoenn",
		shortcut: "gen3"
	};
	this.gen4 = {
		min: 387, max: 493,
		gen: 4,
		name: "Sinnoh",
		shortcut: "gen4"
	};
	this.gen5 = {
		min: 494, max: 649,
		gen: 5,
		name: "Unova",
		shortcut: "gen5"
	};
	this.gen6 = {
		min: 650, max: 721,
		gen: 6,
		name: "Kalos",
		shortcut: "gen6"
	};
	this.gen7 = {
		min: 722, max: 802,
		gen: 7,
		name: "Alola",
		shortcut: "gen7"
	};

	this.kanto = this.gen1;
	this.johto = this.gen2;
	this.hoenn = this.gen3;
	this.sinnoh = this.gen4;
	this.unova = this.gen5;
	this.kalos = this.gen6;
	this.alola = this.gen7;
}


exports.run = (client, message, args) => {
	let num = chance.integer({min:pokedex.min, max: pokedex.max});
	let region = null;
	let user = message.mentions.users.length > 0 ? message.mentions.users.first() : message.author;
	let gen = args[0] ? args[0].toLowerCase() : null;

	// IF gen IS A KEYWORD IN pokedex (except 'min' or 'max')
	if( Object.keys(pokedex).indexOf(gen) > 0 && (gen) !== 'max' && (gen) !== 'min'){
		num = chance.integer({min:pokedex[gen].min, max:pokedex[gen].max})
		region = pokedex[gen].name;
	}

	let msg = ":game_die:  |  "+user+", You got Pokemon **#"+num+"!**"
	if(region) msg += " **("+region+", Gen "+pokedex[gen].gen+")**";
	
	message.channel.sendMessage(msg).then().catch(error=>{
		console.log(error);
	});
}
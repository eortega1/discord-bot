const fs = require('fs');
const prefix = require("../../../config.json").prefix;

exports.run = (client, message, args) => {
	let msg = "```\nStandard Command List\n```\n"

	var promise = new Promise((resolve, reject)=>{
		fs.readdir('./commands', function(err, folders){
			if(err) throw err;

			folders.map((folder, index)=>{
				fs.readdir(`./commands/${folder}`, function(err, commands){
					if(err) throw err;
					msg += `**${index+1}. `+folder.charAt(0).toUpperCase()+folder.slice(1)+" Commands -**`"
						+commands.join("` `")+"\`\n";
					if(index+1 == folders.length) resolve(); 
				});
			});
		});
	});
			
	promise.then(function(err){
		if(err) throw err;
		message.channel.sendMessage(msg);
	});
}
const Chance = require('chance');
const chance = new Chance();

const fortunes = [
	"Reply hazy, try again",
	"Excellent Luck",
	"Good Luck",
	"Average Luck",
	"Bad Luck",
	"Good news will come to you by mail",
	"Better not tell you now",
	"Outlook good",
	"Very Bad Luck",
	"Godly Luck",
	"(YOU ARE BANNED)",
	"YOU JUST LOST THE GAME",
];



exports.run = (client, message, args) => {
	message.channel.sendMessage(':crystal_ball:  |  **'+
		message.author.username+'**, Your fortune: **'+
		fortunes[chance.integer({min:0,max:fortunes.length - 1})]+'**')
	.then().catch((error) => console.log(error));
}
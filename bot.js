'use strict'

const Discord = require("discord.js");
const client = new Discord.Client();
const fs = require('fs');
const path = require('path');
const config = require("./config.json");
const whitelist = require("./whitelist.json");

const Jimp = require('jimp');

client.on("ready", (e) => console.log("BOT IS RUNNING"));
client.on("error", (e) => console.error(e));
// client.on("warn", (e) => console.warn(e));
// client.on("debug", (e) => console.info(e));

client.on("message", message => {
	// BOT CANNOT SEND MESSAGES TO ITSELF
	if(message.author == client.user){return;}
	if(!message.content.startsWith(config.prefix)){return;}


	if(message.content === config.prefix){
		message.channel.sendMessage("Hello, "+message.author+". I am "+client.user).then()
			.catch(error=>{
				console.log(error);
			});
	}
	else{
		let command = message.content.split(' ')[0].toLowerCase();
		command = command.slice(config.prefix.length);

		let args = message.content.split(' ').slice(1);

		try {
			let commandFile = require(`./commands/basic/${command}/${command}.js`);
			commandFile.run(client, message, args);
		} catch(err){
			try {
				let commandFile = require(`./commands/images/${command}/${command}.js`);
				commandFile.run(client, message, args);
			} catch(err){
				console.log(`Could not find command: ${command}`);
			}
		}
	}
});

client.login(config.token);